package com.route4me.loader

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://api.exchangeratesapi.io"

interface Loader {

    companion object {
        val instance: ExchangeEndpointInterface by lazy {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor()).build())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            retrofit.create(ExchangeEndpointInterface::class.java)
        }
    }

}
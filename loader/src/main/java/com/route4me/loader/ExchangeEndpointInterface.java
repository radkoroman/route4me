package com.route4me.loader;

import com.route4me.model.ConvertionData;
import com.route4me.model.RatesRetrospectiveData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ExchangeEndpointInterface {

    @GET("/latest")
    Call<ConvertionData> getExchangeRates(@Query("base") String base);

    @GET("/history")
    Call<RatesRetrospectiveData> getExchangeRatesRetrospective(@Query("base") String base,
                                                               @Query("start_at") String startDate,
                                                               @Query("end_at") String endDate,
                                                               @Query("symbols") String symbols);

}
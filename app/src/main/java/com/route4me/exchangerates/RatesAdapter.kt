package com.route4me.exchangerates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.route4me.model.Rate
import kotlinx.android.synthetic.main.rate_list_item.view.*

class RatesAdapter : RecyclerView.Adapter<RatesAdapter.ViewHolder>() {

    var onItemClick: ((Rate) -> Unit)? = null
    private val ratesList: MutableList<Rate> = arrayListOf()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val rate = ratesList[position]
        holder.name.text = rate.name
        holder.rate.text = rate.value.toString()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.rate_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return ratesList.size
    }

    fun setRates(rates: List<Rate>) {
        ratesList.clear()
        ratesList.addAll(rates)
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = itemView.name
        val rate: TextView = itemView.value
        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(ratesList[adapterPosition])
            }
        }
    }
}
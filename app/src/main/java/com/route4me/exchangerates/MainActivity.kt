package com.route4me.exchangerates

import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.route4me.loader.Loader
import com.route4me.model.ConvertionData
import com.route4me.model.Rate
import com.route4me.model.database.RatesDatabase
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Double
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit


const val USD_CURRENCY = "USD"
const val CURRENCY_PATTERN = "#.##"
private var PRIVATE_MODE = 0
private const val TIMESTAMP = "TIMESTAMP"
const val SELECTED_CURRENCY = "SELECTEED_CURRENCY"
private val TEN_MIN_IN_MILLIS = TimeUnit.MINUTES.toMillis(10)

class MainActivity : AppCompatActivity() {

    private val compositDisposable = CompositeDisposable()
    private var ratesAdapter = RatesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        initRatesList()
        loadRates()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositDisposable.dispose()
    }

    private fun loadRates() {
        if (isRatesFresh()) {
            Loader.instance.getExchangeRates(USD_CURRENCY)
                .enqueue(object : Callback<ConvertionData> {
                    override fun onResponse(
                        call: Call<ConvertionData>,
                        response: Response<ConvertionData>
                    ) {
                        val rates = parseRates(response)
                        saveRatesToDB(rates)
                        ratesAdapter.setRates(rates)
                    }

                    override fun onFailure(call: Call<ConvertionData>, throwable: Throwable) {
                        Toast.makeText(
                            this@MainActivity,
                            "Failed to load currency list.",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                })
        } else {
            Toast.makeText(this, "Read from database.", Toast.LENGTH_LONG).show()
            readRatesFromDB()
        }
    }

    private fun initRatesList() {
        ratesList.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val dividerItemDecoration = DividerItemDecoration(ratesList.context, LinearLayout.VERTICAL)
        ratesList.addItemDecoration(dividerItemDecoration)
        ratesList.adapter = ratesAdapter
        ratesAdapter.onItemClick = { rate ->
            navigateToGraph(rate)
        }
    }

    private fun parseRates(response: Response<ConvertionData>): ArrayList<Rate> {
        val ratesList = ArrayList<Rate>()
        val data: ConvertionData? = response.body()
        data?.rates?.entrySet()?.forEach {
            ratesList.add(
                Rate(
                    it.key,
                    Double.valueOf(DecimalFormat(CURRENCY_PATTERN).format(it.value.asDouble))
                )
            )
        }
        return ratesList
    }

    private fun navigateToGraph(rate: Rate) {
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        val graphFragment: Fragment = GraphFragment()
        val bundle = Bundle()
        bundle.putString(SELECTED_CURRENCY, rate.name)
        graphFragment.arguments = bundle
        transaction.replace(R.id.container, graphFragment)
        transaction.addToBackStack(graphFragment.tag)
        transaction.commit()
    }

    private fun isRatesFresh(): Boolean {
        return System.currentTimeMillis() >= getSharedPreferences(TIMESTAMP, PRIVATE_MODE).getLong(
            TIMESTAMP,
            0
        ) + TEN_MIN_IN_MILLIS
    }

    private fun readRatesFromDB() {
        val disposable =
            Single.fromCallable { RatesDatabase.getInstance(this).rateDao().allRates }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        ratesAdapter.setRates(it)
                        Toast.makeText(
                            this,
                            "Rates list were loaded from the database.",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    },
                    {
                        Toast.makeText(this, "Failed to load rate list.", Toast.LENGTH_LONG)
                            .show()
                    })
        compositDisposable.add(disposable)

    }

    private fun saveRatesToDB(ratesList: ArrayList<Rate>) {
        val disposable =
            Single.fromCallable { RatesDatabase.getInstance(this).rateDao().insertAll(ratesList) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { Toast.makeText(this, "Exhibits list were saved.", Toast.LENGTH_LONG).show() },
                    {
                        Toast.makeText(this, "Failed to load exhibits list.", Toast.LENGTH_LONG)
                            .show()
                    })
        compositDisposable.add(disposable)
        getSharedPreferences(TIMESTAMP, PRIVATE_MODE).edit()
            .putLong(TIMESTAMP, System.currentTimeMillis()).apply()
    }

}

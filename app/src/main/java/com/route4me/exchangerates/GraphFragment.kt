package com.route4me.exchangerates

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.DashPathEffect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.utils.ViewPortHandler
import com.google.gson.JsonObject
import com.route4me.loader.Loader
import com.route4me.model.RatesRetrospectiveData
import kotlinx.android.synthetic.main.graph_fragment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

const val DATE_FORMAT = "yyyy-MM-dd"

class GraphFragment : Fragment() {

    private var currency: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.graph_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        currency = arguments?.getString(SELECTED_CURRENCY).toString()
        initChart()
    }

    @SuppressLint("SimpleDateFormat")
    override fun onResume() {
        super.onResume()
        Loader.instance.getExchangeRatesRetrospective(
            USD_CURRENCY,
            SimpleDateFormat(DATE_FORMAT).format(getStartDate()),
            SimpleDateFormat(DATE_FORMAT).format(Date()),
            currency
        )
            .enqueue(object : Callback<RatesRetrospectiveData> {
                override fun onResponse(
                    call: Call<RatesRetrospectiveData>,
                    response: Response<RatesRetrospectiveData>
                ) {
                    setGraphData(parseRates(response))
                }

                override fun onFailure(call: Call<RatesRetrospectiveData>, throwable: Throwable) {
                    progressBar.visibility = View.GONE
                    Toast.makeText(
                        context,
                        "No exchange rate data is available for the selected currency.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            })
    }

    private fun initChart() {
        val llXAxis = LimitLine(4f, "Index 10")
        llXAxis.lineWidth = 4f
        llXAxis.enableDashedLine(4f, 4f, 0f)
        llXAxis.labelPosition = LimitLine.LimitLabelPosition.RIGHT_BOTTOM
        llXAxis.textSize = 5f

        val xAxis: XAxis = chart.xAxis
        xAxis.enableGridDashedLine(4f, 4f, 0f)
        xAxis.axisMaximum = 6f
        xAxis.axisMinimum = 0f
        xAxis.setDrawLimitLinesBehindData(true)

        val leftAxis: YAxis = chart.axisLeft
        leftAxis.removeAllLimitLines()
        leftAxis.enableGridDashedLine(4f, 4f, 0f)
        leftAxis.setDrawZeroLine(false)
        leftAxis.setDrawLimitLinesBehindData(false)
        chart.axisRight.isEnabled = false
    }

    private fun setGraphData(data: SortedMap<String, Double>) {
        val values = mutableListOf<Entry>()
        for ((index, value) in data.keys.withIndex()) {
            values.add(Entry(index.toFloat(), data[value]?.toFloat()!!, value))
        }
        val lineDataSet = LineDataSet(values, currency)
        lineDataSet.setDrawIcons(false)
        lineDataSet.enableDashedLine(4f, 2f, 0f)
        lineDataSet.enableDashedHighlightLine(4f, 2f, 0f)
        lineDataSet.color = Color.DKGRAY
        lineDataSet.setCircleColor(Color.DKGRAY)
        lineDataSet.lineWidth = 1f
        lineDataSet.circleRadius = 3f
        lineDataSet.setDrawCircleHole(false)
        lineDataSet.valueTextSize = 9f
        lineDataSet.setDrawFilled(true)
        lineDataSet.formLineWidth = 1f
        lineDataSet.formLineDashEffect = DashPathEffect(floatArrayOf(4f, 2f), 0f)
        lineDataSet.formSize = 15f

        val dataSets = ArrayList<ILineDataSet>()
        lineDataSet.fillColor = Color.DKGRAY
        dataSets.add(lineDataSet)
        val chartData = LineData(dataSets)
        chart.data = chartData
        chart.data.setValueFormatter(object : ValueFormatter() {
            override fun getFormattedValue(
                value: Float,
                entry: Entry,
                dataSetIndex: Int,
                viewPortHandler: ViewPortHandler
            ): String {
                return "$value : " + entry.data.toString()
            }
        })
        lineDataSet.setDrawFilled(true)
        chart.data.notifyDataChanged()
        chart.invalidate()
        progressBar.visibility = View.GONE
    }

    private fun parseRates(response: Response<RatesRetrospectiveData>): SortedMap<String, Double> {
        val ratesMap = HashMap<String, Double>()
        val data: RatesRetrospectiveData? = response.body()
        data?.rates?.entrySet()?.forEach {
            val date = it.key
            (it.value as JsonObject).entrySet().forEach {
                with(ratesMap) {
                    put(date, it.value.asDouble)
                }
            }
        }
        return ratesMap.toSortedMap(compareBy<String> { it.length }.thenBy { it })
    }

    private fun getStartDate(): Date {
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        calendar.add(Calendar.DAY_OF_YEAR, -7)
        return calendar.time
    }
}
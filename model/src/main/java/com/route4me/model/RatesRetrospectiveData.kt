package com.route4me.model

import com.google.gson.JsonObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class RatesRetrospectiveData: Serializable {

    @SerializedName("rates")
    @Expose
    var rates: JsonObject? = null

    @SerializedName("base")
    @Expose
    var base: String = ""

    @SerializedName("start_at")
    @Expose
    var startAt: String = ""

    @SerializedName("end_at")
    @Expose
    var endAt: String = ""
}
package com.route4me.model.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.route4me.model.Rate
import com.route4me.model.SingletonHolder


@Database(entities = [Rate::class], version = 1)
abstract class RatesDatabase : RoomDatabase() {

    abstract fun rateDao(): RateDao

    companion object : SingletonHolder<RatesDatabase, Context>({
        Room.databaseBuilder(it.applicationContext,
            RatesDatabase::class.java, "rate.db")
            .build()
    })
}
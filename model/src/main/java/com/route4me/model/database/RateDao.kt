package com.route4me.model.database

import androidx.room.*
import com.route4me.model.Rate

@Dao
interface RateDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(rates: List<Rate>)

    @Delete
    fun delete(rate: Rate?)

    @get:Query("SELECT * FROM rates")
    val allRates: List<Rate>

    @Query("SELECT * FROM rates WHERE name LIKE :name")
    fun getRateByName(name: String?): List<Rate?>?
}
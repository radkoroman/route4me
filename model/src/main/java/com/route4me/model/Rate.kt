package com.route4me.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "rates")
data class Rate(
    @PrimaryKey val name: String,
    @ColumnInfo(name = "value") val value: Double
)
package com.route4me.model

import com.google.gson.JsonObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ConvertionData: Serializable {

    @SerializedName("rates")
    @Expose
    var rates: JsonObject? = null

    @SerializedName("base")
    @Expose
    var base: String = ""

    @SerializedName("date")
    @Expose
    var date: String = ""
}